<?php
namespace App\Http\Controllers;

use DB;
use Laravel\Lumen\Routing\Controller;
use Illuminate\Http\Response;

class BaseController extends Controller
{   

    public function jsonResponse($error,$result="",$message=""){
        if($message == "paramError") $message = $result->first();
        return ["error"=>$error,"result"=>$result,"message"=>$message];
    }

    public function tableConfig($table){
        return require "../storage/app/config/$table.php";
    }

    //http请求函数
    public function httpRequest($url,$data=null,$header=null)
    {   
        // var_dump($url);
        // var_dump($header);exit;
        $curl = curl_init($url);
        curl_setopt ($curl, CURLOPT_HTTPHEADER, $header );
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,false);  //1：回复内容 0：输出内容
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,false); 
        curl_setopt ($curl, CURLOPT_USERAGENT,'PTime.Activity/1.0');
        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1); //模拟post方式
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data)); //对数组进行处理
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //执行事务
        $output = curl_exec($curl);
        //关闭
        curl_close($curl);
        //输出内容
        return $output; 
    }
    
    //id获取用户信息
    public function userInfo($user_id,$header)
    {   
        $userApi = $_ENV["PTIME_URL"]."/user/$user_id";
        $userResult = $this->httpRequest($userApi,[],$header);
        $userInfo = json_decode($userResult);
        return $userInfo;
    }

    //oauth授权
    public function oauth2($redirect_uri,$appkey)
    {   
        $oauthUrl = $_ENV["OAUTH_URL"]."/oauth2/auth?client_id=$appkey&state=$redirect_uri&response_type=code";
        Header("Location: $oauthUrl");exit;
    }

    //campus授权
    public function getCourse($user_id,$header)
    {
        $courseUrl = $_ENV["PTIME_URL"]."/user/$user_id/system_cat/edu/table";
        $courseResult = $this->httpRequest($courseUrl,['1'=>'1'],$header);
        return $courseResult;
    }

    //教务计算当前周
    public function schoolWeekNow()
    {
        //计算当前周
        $weekStartArr = Config('ptime.week_start_arr');
        $unixTimeNow = time();
        $tempWeekCounter = 1;
        $tempWeekArr = array();
        $weekStartArrLength = count($weekStartArr);
        $weekStartArrLength--;
        //计算每周开始和结束时间戳
        foreach( $weekStartArr as $key => $value ){
            $tempNextKey = $key+1;
            if( $key < $weekStartArrLength ){
                $tempWeekArr[$tempWeekCounter] = array(
                    '0' => strtotime($value),
                    '1' => strtotime($weekStartArr[$tempNextKey]),
                    );
            }
            $tempWeekCounter++;
        }
        //计算当前周
        foreach( $tempWeekArr as $key => $value){
            if( $unixTimeNow >= $value[0] && $unixTimeNow <= $value[1] ){
                $targetSchoolWeek = $key;
            }else{
                continue;
            }
        }
        return $targetSchoolWeek;
    }

    public function edImport($user_id,$object)
    {
        $importData = [
            "user_id" => $user_id,
            'object'  => $object,
        ];
        $delteImport = DB::table('ed_import')->where('user_id',$user_id)->where('object',$object)->delete();
        $importResult = DB::table('ed_import')->insert($importData);
    }
}