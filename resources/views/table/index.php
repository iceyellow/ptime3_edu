<?php 
    // 通用变量
    $title        = "拾光&nbsp;&bull;&nbsp;课表";
    $description  = "课表导入,查询";
    $header       = "拾光&nbsp;&bull;&nbsp;课表";
    $img          = "http://source.timepicker.cn/uploads/default/edu.png-120";
    $domain = env('APP_ENV') == 'product' ? 'timepicker.cn' : 'lucky.com';
    $campus_url = env('APP_ENV') == 'product' ? env('OAUTH_URL') : 'http://ptime.lucky.com';
    // var_dump($result);exit();

    // 课表变量
    $weeks = $result['school_week_all'];
    $week_now = $result['school_week_now'];
    $week_table = $result['table_week'];
    $days = $result['week_day'];
    $month = $result['month'];
    $table = $result['table'];
    $week_day = ['一','二','三','四','五','六','日'];
    $background = ['#EF8F82','#7DC0DA','#ACE07C','#F9C264'];
?>
<?php include("../resources/views/header.php");?>
<!-- 课表 -->
<?php if(!$result['is_empty']):?>
    <table cellspacing='0'>
        <thead>
            <tr>
                <th class="week_box">
                    <div class="<?=$week_table==$week_now?'week_now current':'week_now'?>"><?=$month."月"?></div>
                </th>
                <?php for($i=0;$i<count($days);$i++):?>
                    <th class="<?=($days[$i]==$result['date'])?'current':''?>">
                        <?php
                            $arr = explode('-', $days[$i]);
                            $month = $arr[0];
                            $day = $arr[1];
                        ?>
                        <?php if($i>0 && substr($days[$i],0,2) > substr($days[$i-1],0,2)):?>
                        <div><?=$month.'月'?></div>
                        <?php else:?>
                        <div><?=$day?></div>    
                        <?php endif?>
                        <div>周<?=$week_day[$i]?></div>
                    </th>
                <?php endfor?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($table as $key1 => $value) :?>
                <tr>
                    <td><?=explode(",",$key1)[0]?></td>
                    <?php foreach ($value as $key2 => $class_day) :?>
                        <td rowspan="2" onclick="showClassInfo('<?=$key1?>','<?=$key2?>')">
                            <?php foreach ($class_day as $class_item) :?>
                                <?php if ($class_item['is_intime']==1) :?>
                                <div class="classBox" style="background:<?=$background[rand(0,3)]?>">
                                    <div class="subject_name"><?=$class_item['subject_name']?></div>
                                    <div class="subject_addr"><?=$class_item['addr']?></div>
                                </div>
                                <?php break;endif?>
                            <?php endforeach?>
                            <?php if(count($class_day)>1):?>
                                <div class="num"><?=count($class_day)?></div>
                            <?php endif?>
                        </td>
                    <?php endforeach?>
                </tr>
                <tr>
                    <td><?=explode(",",$key1)[1]?></td>
                </tr>
            <?php endforeach?>
        </tbody>
    </table>
<?php else :?>
    <div class="emptyPage">
        <img src="/img/edu.png">
        <p class="mcolor">最近导入无课程！</p>
        <span class="button mcolor mid" onclick='reImport()'>重新导入</span>
    </div>
<?php endif?>


<!-- 教务授权iframe -->
<div class="shadow" id="iframeBox" onclick="toggle('iframeBox')" style="display: none;background: rgba(0,0,0,.4);position: fixed;top: 0;left: 0;width: 100%;height: 100%;color: #fff;z-index: 2;" >
    <iframe id="iframe" name="iframe" src="">  
    </iframe>
</div>

<!-- 右下角menu -->
<span class="send-button circular button-menu" id="menu" onclick="menu()"><?=$result['table_week']?></span>
<span class="send-button circular button-weeks" id="weeks" onclick="weeksBoxShow()">周</span>
<span class="send-button circular button-reload" id="reload" onclick='reImport()'><div class="reload"></div></span>
<div class="shadow" id="weeksBox" onclick="toggle('weeksBox')">
    <ul id="weekSelect" class="classInfo" style="text-align:center;padding:10px 0;">
        <?php foreach ($weeks as $key => $value) :?>
            <?php if($value==$result['school_week_now']):?>
            <li value="<?=$value?>"><a href="/table?week=<?=$value?>"><?=($value>0) ? '第'.$value.'周(教学周)' : '假期第'.substr($value,1).'周'?></a></li>
            <?php elseif($value==$result['table_week']) :?>
            <li  value="<?=$value?>"><a href="/table?week=<?=$value?>" style="color:#329da3"><?=($value>0) ? '第'.$value.'周' : '假期第'.substr($value,1).'周'?></a></li>
            <?php else :?>
            <li value="<?=$value?>"><a href="/table?week=<?=$value?>"><?=($value>0) ? '第'.$value.'周' : '假期第'.substr($value,1).'周'?></a></li>
            <?php endif ?> 
        <?php endforeach?>
    </select>
</div>

<!-- 课表详情对话框 -->
<div class="shadow" id="classInfoBox" onclick="toggle('classInfoBox')">
    <div class="classInfo" id="classInfo">
        <section>
        </section>
    </div>
</div>

<?php include("../resources/views/footer.php");?>

<script type="text/javascript">
    document.domain = '<?=$domain?>';
    var is_iframe;
    function iframeShow(url){
        menu();
        is_iframe = true;
        $("#iframe").attr("src",url);
        $("#iframeBox").show();
    }
    function iframeCallback(){
        reImport();
    }
    function reImport(){
        loadingShow("导入课表...");
        $.ajax({
            url:'/api/table',
            type:'post',
            dataType:'json',
            success:function(data){
                loadingHide();
                if(!data.error){
                    getData();
                }else{
                    toast(data.message);
                    iframeShow("<?=$campus_url?>/campus/oauth2/auth?state=1&user_id=<?=$result['user_id']?>&system_cat=edu")
                }
            },
            error:function(data){
                loadingHide();
                toast("重新导入接口Ajax error");
            }
        })
    }
    function getData(){
        var ua = navigator.userAgent.toLowerCase();
        if (ua.match('ptime') == "ptime") {
            $.ajax({
                url:'/api/data/table',
                type:'get',
                dataType:'json',
                success:function(data){
                    loadingHide();
                    if(!data.error){
                        var payload = {
                            type: 'ClassTableImport',
                            content: data
                        };
                        try  
                        {  
                           var ua = navigator.userAgent.toLowerCase();
                            if (ua.match('ptime') == "ptime") {
                                var payload = {
                                    type: 'ClassTableImport',
                                    content: data
                                };
                                if (ua.match('androidweb') == "androidweb") {
                                    window.AndroidJsBridge.postMessage(JSON.stringify(payload));
                                }else{
                                    window.webkit.messageHandlers.webViewApp.postMessage(payload);
                                };
                                return true;
                            }else{
                                location.reload();
                            }
                        }  
                        catch (e) {  
                            console.log('share函数报错' + e.message);  
                        }
                    }else{
                        toast(data.message);
                    }
                },
                error:function(data){
                    loadingHide();
                    toast("重新导入接口Ajax error");
                }
            })
        }else{
            location.reload();
        }
    }

    // 查看课表详情
    function showClassInfo(class_num,day){
       $(".classInfo section").html('');
        var table = '<?=json_encode($table)?>';
        var classData = JSON.parse(table)[class_num][day];
        var tpl = '';
        if (classData.length) {
            for (var i = classData.length - 1; i >= 0; i--) {
                tpl += '<div class="item is_intime_'+classData[i]['is_intime']+'" style="box-shadow:none;">\
                            <div class="subject_name">科目：'+classData[i]['subject_name']+'</div>\
                            <div class="addr">\
                                地点：'+classData[i]['addr']+'</br>\
                                教师：'+classData[i]['teacher_name']+'</br>\
                                时间：'+classData[i]['week_start']+'-'+classData[i]['week_end']+'周&nbsp;第'+classData[i]['class_n']+'节\
                            </div>\
                        </div>';
            };
            $(".classInfo section").append(tpl);
            $("#classInfoBox").show();
        };
    }
    // 切换周
    function switchWeek(){
        window.location.href="/table?week="+$("#weekSelect").val();
    }

    // menu
    function animateName(id,type){
        $("#"+id).addClass("animated "+type);
        setTimeout(function(){
            $("#"+id).removeClass("animated "+type);
        },1000);
    }
    function menu(){
        animateName('menu','jello');
        if($("#weeks").css('display')!="none"){
            $("#menu").html('<?=$result["table_week"]?>');
            animateName('weeks','rollOut');
            animateName('reload','rollOut');
            $("#weeksBox").hide();
            setTimeout(function(){
                $("#weeks,#reload").hide();
            },1000)

        }else{
            $("#menu").html('<div class="close"></div>');
            $("#weeks,#reload").show();
            animateName('weeks','rubberBand');
            animateName('reload','rubberBand');
        }
    }
    function weeksBoxShow(){
        menu();
        if($("#weeksBox").css('display')!="none"){
            $("#weeksBox").hide();
        }else{
            if ($(window).width()>480) {
                $("#weeksBox").show();
            }else{
                $("#weekSelect").click();
            };
        }
    }
</script>