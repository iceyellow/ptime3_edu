<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <title><?=$title?></title>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="format-detection"content="telephone=no, email=no" />
    <meta name="renderer" content="webkit">

    <meta name="title" content="<?=$title?>" />
    <meta name="description" content="<?=$description?>" />
    <meta name="img" content="<?=isset($img)?$img:''?>" />
    <!--icon-->
    <link rel="shortcut icon" href="http://source.timepicker.cn/uploads/default/edu.png-120" />
    <!--拾光css-->
    <link rel="stylesheet" href="http://source.timepicker.cn/static/css/ptime_v0.4.min.css">
    <link rel="stylesheet" type="text/css" href="/css/edu.css">
</head>
<body class="agent-<?=getUserAgent()?>">
    <!--头部-->
    <header class="bg-mcolor fixed top" >
        <h1><?=$header?></h1>
        <!-- <span class="button top-right white" onclick="share()">分享</span> -->
    </header>

    <!-- php函数 -->
    <?php
        //获取用户代理名称 ptimeios,ptimeandroid,wechat,mqq,other
        function getUserAgent(){
            if(     strpos($_SERVER['HTTP_USER_AGENT'], 'PTime.IOSWeb') !== false):
                $userAgent = "ptimeios";
            elseif( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false):
                $userAgent = "wechat";
            elseif( strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== false):
                $userAgent = "ptimeandroid";
            elseif( strpos($_SERVER['HTTP_USER_AGENT'], 'MQQBrowser')!== false):
                $userAgent = "mqq";
            else:
                $userAgent = "other";
            endif;
            return $userAgent;
        }

    ?>