// 某个对象toggle显示
function toggle(id){
	if ($("#"+id).css("display")=='block') 
		$("#"+id).hide();
	else
		$("#"+id).show();
}
// loading
function loadingShow(message){
	$("#loading-box h4").html(message);
	if ($("#loading-box").css("display")!='block') 
		$("#loading-box").show();
}
function loadingHide(){
	$("#loading-box").hide();
}